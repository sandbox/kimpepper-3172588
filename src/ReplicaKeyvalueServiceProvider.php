<?php

namespace Drupal\replica_keyvalue;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Alters container definitions for Replica KeyValue.
 */
class ReplicaKeyvalueServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $container->setParameter('factory.keyvalue', ['default' => 'keyvalue.replica']);
  }

}
