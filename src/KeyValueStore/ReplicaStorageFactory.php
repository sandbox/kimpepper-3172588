<?php


namespace Drupal\replica_keyvalue\KeyValueStore;


use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\ReplicaKillSwitch;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;

class ReplicaStorageFactory implements KeyValueFactoryInterface {

  /**
   * The replica database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $replica;

  /**
   * The replica kill switch.
   *
   * @var \Drupal\Core\Database\ReplicaKillSwitch
   */
  protected $replicaKillSwitch;

  /**
   * The serialization class to use.
   *
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $serializer;

  /**
   * The database connection to use.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs this factory object.
   *
   * @param \Drupal\Component\Serialization\SerializationInterface $serializer
   *   The serialization class to use.
   * @param \Drupal\Core\Database\Connection $connection
   *   The Connection object containing the key-value tables.
   * @param \Drupal\Core\Database\Connection $replica
   *   The replica connection object.
   * @param \Drupal\Core\Database\ReplicaKillSwitch $replicaKillSwitch
   *   The replica kill switch.
   */
  public function __construct(SerializationInterface $serializer, Connection $connection, Connection $replica, ReplicaKillSwitch $replicaKillSwitch) {
    $this->serializer = $serializer;
    $this->connection = $connection;
    $this->replica = $replica;
    $this->replicaKillSwitch = $replicaKillSwitch;
  }

  /**
   * {@inheritdoc}
   */
  public function get($collection) {
    return new ReplicaStorage($collection, $this->serializer, $this->connection, $this->replica, $this->replicaKillSwitch);
  }

}
