<?php

namespace Drupal\replica_keyvalue\KeyValueStore;

use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\ReplicaKillSwitch;
use Drupal\Core\KeyValueStore\DatabaseStorage;

class ReplicaStorage extends DatabaseStorage {

  /**
   * The replica database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $replica;

  /**
   * The replica kill switch.
   *
   * @var \Drupal\Core\Database\ReplicaKillSwitch
   */
  protected $replicaKillSwitch;

  /**
   * ReplicaStore constructor.
   *
   * @param string $collection
   *   The collection name.
   * @param \Drupal\Component\Serialization\SerializationInterface $serializer
   *   The serializer.
   * @param \Drupal\Core\Database\Connection $connection
   *   The primary database connection.
   * @param \Drupal\Core\Database\Connection $replica
   *   The replica database connection.
   * @param \Drupal\Core\Database\ReplicaKillSwitch $replicaKillSwitch
   *   The replica kill switch.
   * @param string $table
   *   (optional) The table name.
   */
  public function __construct($collection, SerializationInterface $serializer, Connection $connection, Connection $replica, ReplicaKillSwitch $replicaKillSwitch, $table = 'key_value') {
    parent::__construct($collection, $serializer, $connection, $table);
    $this->replica = $replica;
    $this->replicaKillSwitch = $replicaKillSwitch;
  }

  public function has($key) {
    try {
      return (bool) $this->replica->query('SELECT 1 FROM {' . $this->replica->escapeTable($this->table) . '} WHERE [collection] = :collection AND [name] = :key', [
        ':collection' => $this->collection,
        ':key' => $key,
      ])->fetchField();
    }
    catch (\Exception $e) {
      $this->catchException($e);
      return FALSE;
    }
  }

  public function getMultiple(array $keys) {
    $values = [];
    try {
      $result = $this->replica->query('SELECT [name], [value] FROM {' . $this->replica->escapeTable($this->table) . '} WHERE [name] IN ( :keys[] ) AND [collection] = :collection', [':keys[]' => $keys, ':collection' => $this->collection])->fetchAllAssoc('name');
      foreach ($keys as $key) {
        if (isset($result[$key])) {
          $values[$key] = $this->serializer->decode($result[$key]->value);
        }
      }
    }
    catch (\Exception $e) {
      // @todo: Perhaps if the database is never going to be available,
      // key/value requests should return FALSE in order to allow exception
      // handling to occur but for now, keep it an array, always.
    }
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function getAll() {
    try {
      $result = $this->replica->query('SELECT [name], [value] FROM {' . $this->replica->escapeTable($this->table) . '} WHERE [collection] = :collection', [':collection' => $this->collection]);
    }
    catch (\Exception $e) {
      $this->catchException($e);
      $result = [];
    }

    $values = [];
    foreach ($result as $item) {
      if ($item) {
        $values[$item->name] = $this->serializer->decode($item->value);
      }
    }
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function set($key, $value) {
    $this->replicaKillSwitch->trigger();
    parent::set($key, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function setIfNotExists($key, $value) {
    $this->replicaKillSwitch->trigger();
    parent::setIfNotExists($key, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function rename($key, $new_key) {
    $this->replicaKillSwitch->trigger();
    parent::rename($key, $new_key);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteMultiple(array $keys) {
    $this->replicaKillSwitch->trigger();
    parent::deleteMultiple($keys);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAll() {
    $this->replicaKillSwitch->trigger();
    parent::deleteAll();
  }

}
