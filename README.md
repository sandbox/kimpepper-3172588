# Replica Key Value

This module provides a replica database keyvalue backend.

KeyValue `gets` will attempt to use the replica database connection if available.
Otherwise, the primary database will be used as normal.

KeyValue `sets` will trigger the 'replica kill switch' forcing the primary database
to be used for the remainder of the current request.

## Setup

The module with alter the container parameter definition setting as default:

```
parameters:
  factory.keyvalue:
    default: keyvalue.replica
```

## Configuration

You may wish to adjust the `maximum_replication_lag` setting from its default of
`300` seconds.

```
$settings['maximum_replication_lag'] = 5;
```
